# Wikimedia Enterprise

This repository contains the source code for the Wikimedia Enterprise project, organized into four main directories:

1. [/api](/api/) - contains the APIs exposed by the project

1. [/docker](/docker/) - stores the shared Docker configuration

1. [/general](/general/) - includes libraries shared across the entire project

1. [/services](/services/) - houses the individual services responsible for running the project
