grpcio >= 1.51.1
grpcio-tools >= 1.51.1
protobuf >= 4.21.12
black >= 24.8.0
flake8 >= 4.0.1
dnspython >= 2.3.0
kafka-python >= 2.0.2
boto3
psycopg2-binary
redis >= 4.6.0
