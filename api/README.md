# Wikimedia Enterprise APIs

This directory houses the APIs exposed by WME:

1. [/main](/api/main/) - serves data from the object store

1. [/auth](/api/auth/) - handles user authentication and authorization

1. [/realtime](/api/realtime/) - provides a real-time feed of changes
