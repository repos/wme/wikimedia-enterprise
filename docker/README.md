# Wikimedia Enterprise Docker Configuration

This directory houses the general Docker configuration for the project:

1. [/handlers](/docker/handlers/) - provides the default configuration for Go-based microservices

1. [/mock](/docker/mock/) - contains the Docker container for mock generation
